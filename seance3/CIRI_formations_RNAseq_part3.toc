\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Objectif de la s\IeC {\'e}ance}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Chargement du package et environnement R}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Pr\IeC {\'e}paration des donn\IeC {\'e}es}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Tableau de comptage}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Variables factorielles}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Analyse \IeC {\`a} un facteur}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Analysis}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Volcano Plots}{9}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}MA-Plot, relation entre expression moyenne et log-fold change}{10}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Plot g\IeC {\`e}ne par g\IeC {\`e}ne}{10}{subsection.3.4}
\contentsline {section}{\numberline {4}Analyse \IeC {\`a} deux facteurs}{13}{section.4}
\contentsline {section}{\numberline {5}Analyse fonctionnelle}{16}{section.5}
\contentsline {section}{\numberline {6}Conclusions}{17}{section.6}
