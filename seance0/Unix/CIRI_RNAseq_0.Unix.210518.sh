##########################################################
### Atelier prise en main de la ligne de commande Unix ###
##########################################################

### -------------------------
### Installation préliminaire
### -------------------------
# Télécharger le dossier Atelier_Unix.zip
# si vous êtes sous Linux ou MacOS, 
#     - placer le dans votre home (le dossier de départ de vos fichiers personnels) et décompresser le. 
#     - Linux : pour ouvrir votre terminal, tapez Ctrl-Alt-T
#     - MacOS : pour ouvrir votre terminal, Menu Applicaton -> Utilities -> Terminal
# si vous êtes sous Windows ou si vous préférez travailler sur un émulateur en ligne : 
#    - Créer un compte à l'IFB, aller dans "l'appliance" CIRI RNA-seq formation, créer une virtual machine, ouvrez la console Rstudio, 
#    - cliquez sur l'onglet "Terminal" (panel de gauche). 
#    - écrire : mkdir 0.Unix && cd 0.Unix , et tapez Enter
#    - cliquez sur l'Onglet "Files"  (panel en bas à droite), puis sur l'icone "Upload"
#    - vous devez avoir dans Target directory : ~/0.Unix, recherchez dans Parcourir... le dossier zippé Unix_data.zip, et cliquez OK
# Si vous avez ouvert ce fichier dans Sublime Text : 
#    - tout à fait en bas à droite de la fenêtre cliquez sur "Plein text" est scroller jusqu"à Bourne Again Shell (bash). 
#    - (vous pouvez aussi utiliser le menu View -> Syntax -> Bourne Again Shell (bash)).

# je suppose que vous avez lu le début du document que nous vons avoins envoyé et que vous savez ce qu'est un shell, et que bash est le shell que nous allons utiliser 

# Dans ce document, tout ce qui suit un '#' est un commentaire, bash l'ignore. 


### ----------------------------------------
### Navigation dans votre système de fichier
### ----------------------------------------

### Sous quel nom de d'utilisateur êtes-vous loggé ? 

whoami             ### who am I ?


### Notion de répertoire de travail, de répertoire absolu et de répertoire relatif
# l'adresse absolue de votre répertoire de travail courant peu être retrouvé par : 

pwd                ### "print working directory"

# Au démarrage d'une session votre répertoire de  travail est votre "home" 


### lister le contenu d'un repertoire 
 
ls                 ### "list". par défaut liste le répertoire de travail

ls 0_Unix          ### Utilisez l'autocomplétion : tapez ls A <tab>

LS                 ### ne marche pas : Unix est sensible à la casse

# les commandes possèdes des options altérant leur comportement 

ls -l              ### "long" (une option courte). Analyse de la version longue. "." = repertoire de travail, ".." = repertoire parent

ls ..              ### le contenu du répertoire parent

ls --version       ### un exemple d'option longue 

ls -a              ### y compris les fichiers cachés

ls -a -l           ### on peut combiner les options 

ls -alh O_Unix     ### écriture racourcie + affiche les taille de fichier dans format lisible "human" + Options avant arguments

# jouez avec les flèches vers le haut et le bas, pour parcourir l'historique des commandes. 


### Changement de répertoire de travail : 

pwd

cd 0_Unix          ### adresse relative

pwd                ### pour voir le changement 

cd ; pwd           ### on peut juxtaposer deux commande sur une seule ligne';'. Repertoire par défaut = home

# ecrire "cd " puis glisser-déposer le repertoire Atelier_Unix depuis la fenêtre du Finder

cd '/home/<login>/0_Unix' ### chemin absolu. Les guillements protègent les espaces potentiels dans le nom de fichier

ls                 ### vous êtes bien dans 0_Unix

ls ~               ### ~ est un raccourci pour votre home. Observez l'invite. 


### en savoir plus sur les commandes (ne marche pas à l'IFB)

man ls            ### tapez "q" pour sortir (ne marche pas à l'IFGB)

### -------------------------
### Manipuler des répertoires
### -------------------------

# On va travailler dans le répertoire atelier : S'y placer et vérifier l'état.

pwd

cd ~/O_Unix                  ### si nécessaire

ls -l


### créer un répertoire 

mkdir Dir1; ls               ### "make directory"

mkdir Dir2 /home/<login>/0_Unix/Dir3; ls ### accepte une liste

mkdir Dir4/Dir41/Dir411/Dir4111 ### Pas OK : le dépertoire parental doit exister

mkdir -p Dir4/Dir41/Dir411/Dir4111 ### "parent" : crée les répertoires parentaux à la volée.
ls -l
ls -l Dir4
ls -l Dir4/Dir41/
ls -l Dir4/Dir41/Dir411
ls -l Dir4/Dir41/Dir411/Dir4111

### supprimer un répertoire

rmdir Dir1 Dir3; ls          ### "remove directory", accepte une liste

rmdir Dir4                   ### pas OK : seuls les répertoires vides peuvent être suppimés

rmdir -p Dir4/Dir41/Dir411/Dir4111 ### avec -p, supprime aussi tous les répertoires parentaux vides


### -----------------------------------------------------
### Ecrire, lire et concaténer des fichiers, Redirections
### -----------------------------------------------------

# on se crée un dossier pour des tests, et on va dedans 

mkdir Test; cd Test


### notion de STDIN, STDOUT, STDERR 


### écrire à l'écran

echo hello word               ### prend dans STDIN, envoie sur STDERR. Ajoute un retour à la ligne.


### Ecrire et lire dans un fichier : Notion de redirection 

echo Hello word > Hello_word.txt ### redirige STDOUT vers un fichier (créé à la volée) 
ls

# lire dans un fichier 

cat Hello_word.txt           ### envoie le fichier sur STDOUT

echo Bonjour le monde > Hello_word.txt; cat Hello_word.txt ### écrase l'ancienne version 

echo hello word >> Hello_word.txt; cat Hello_word.txt ### >>  = mode "append"

ls -l > Directory_content.txt; cat Directory_content.txt ### marche pour toute instruction qui écrit dans STDOUT

ls -l > Directory_content.txt; cat -n Directory_content.txt ### "numbers" : ajoute des n° de lignes

# par défaut, cat utilise STDIN. 

cat < Hello_word.txt ### < fait une redirection du fichier du fichier vers STDIN (même chose que cat Hello_Word.txt)

cat < Hello_word.txt >> Directory_content.txt ### on peut associer les redirections.
cat Directory_content.txt

### Concaténer des fichiers

cat Hello_word.txt
cat Hello_word.txt Directory_content.txt ### en fait cat "concacenate" concatène les fichiers en argument, et les renvoie sur STDOUT

cat Hello_word.txt Directory_content.txt > Hello_Content.txt ### permet de sauvegarder la concatenation
ls -l; cat -n Hello_Content.txt


### -------------------------------
### Remommer, déplacer, copier un fichier 
### -------------------------------

cd ..; pwd ### vérifier qu'on est dans 0_Unix


### remommer et déplacer sont une seule et même chose : modification de l'adresse du fichier de la table des fichiers, sans toucher aux données.

mv Unix_data/28035.66.fna Unix_data/Staph_lugd.28035.66.fna; ls ### "move" renomme le fichier

mv Unix_data/Staph_lugd.28035.66.fna ../Test ### dépace dans Test
ls Unix_data; ls Test

mv Test/Staph_lugd.28035.66.fna /home/<login>/0_Unix/Unix_data/28035.66.fna ### on peut déplacer et renommer en même temps
ls Unix_data; ls Test 

mv Dir2 Test; ls; ls Test    ### on peut aussi déplacer un repertoire s'il est vide
mv Test/Dir2 .               ### on revient à la sitaution antérieure 
mv Test Dir2 ; ls; ls Dir2; ls Dir2/Test ### est même s'il n'est pas vide 

### organissons les fichiers de façon un peu plus propre

cd Unix_data
mkdir Genomes GTF_GFF                                                                     ### accepte plusieurs dossiers
mv 28035.66.fna Genomes
mv 28035.66.PATRIC.gff gencode.v35.chr20-22_patch_hapl_scaff.basic.annotation.gtf GTF_GFF ### accepte plusieurs fichiers

mv ../Dir2/Test/ ..
rmdir Dir2/


### copier un fichier 

cp Genomes/28035.66.fna Genomes/Staph_lugd.28035.66.fna ### "copy". copie en renommant 

cp Genomes/28035.66.fna GTF_GFF/28035.66.PATRIC.gff ../Test ### copie 2 fichiers, dans un autre repertoire sans renommer

cp Genomes ../Test           ### pas OK, on ne peut pas copier une répertoire (comme cela)

cp -r Genomes ../Test        ### "recursive" : copie le répertoire et tout son contenu, récursivement

# se méfier de copier !


### supprimer un fichier

# c'est une opération dangereuse

cd ..; ls Test; 
ls Test/Genomes

rm Test/Genomes/28035.66.fna ### "remove"

rm Test/Directory_content.txt Test/28035.66.PATRIC.gff ### accepter plusieurs fichiers

rm -R Test/Genomes/          ### "recursive". avec -R delete le répertoire et tout son contenu. WARNING : très dangeureux !

rm Test/Hello*.txt           ### partout où bash accepte un fichier, on eut utiliser un "glob" ( * ; ? ; [ABCD] ; ... ) 


### ------------------------------------------------
### Explorer un fichier texte, introdution aux pipes 
### ------------------------------------------------

### statistiques sur un fichier

cd Unix_data/GTF_GFF/; ls -l

wc gencode.v35.chr20-22_patch_hapl_scaff.basic.annotation.gtf ### "word count" : donne nb de lignes , de mots, d'octets

wc -l gencode.v35.chr20-22_patch_hapl_scaff.basic.annotation.gtf ### nb de lignes

wc -w gencode.v35.chr20-22_patch_hapl_scaff.basic.annotation.gtf ### nb de mots

wc -w *.*                    ### pour tous les fichiers, + calcule la somme totale


### lire un fichier 

cat 28035.66.PATRIC.gff      ### trop long. Pas top ! 

more 28035.66.PATRIC.gff     ### mieux : utiliser <enter> ou <space> pour voir plus loin, quitter par 'q'

less 28035.66.PATRIC.gff     ### encore mieux : utilisez les flèches de direction, la souris, etc. quitter par 'q'


### lire le début ou la fin d'un fichier 

head 28035.66.PATRIC.gff     ### montre les 10 premières lignes 

tail 28035.66.PATRIC.gff     ### montre les 10 dernières lignes 

head -n 25 28035.66.PATRIC.gff ### montre les 25 premières lignes (même chose pour tail )

### Les pipes
# toutes ces commandes prennent leurs entrée et leur sortie par défaut dans STDIN et STDOUT, un pipe permet de créer un petit pipeline à la volée

cat -n 28035.66.PATRIC.gff | head -n 20
cat -n 28035.66.PATRIC.gff | head -n 40 | tail -n 20      ### montre les lignes 21 à 40

head -n 40 28035.66.PATRIC.gff | tail -n 20 | wc  ### fait les statistiques sur les lignes 21 à 40 

head -n 1000 28035.66.PATRIC.gff | cat -n > 28035.66.PATRIC.n1-1000.gff ###  récupère les 1000 premières lignes de hg38.fa, 
                                                                        ### ajoute des numéros de lignes et sauvegarde dans un nouveau fichier.
ls; less 28035.66.PATRIC.n1-1000.gff             ### pour vérifier

rm 28035.66.PATRIC.n1-1000.gff

### ----------------------------------------------------
### Rechercher un motif et filtrer dans un fichier texte
### ----------------------------------------------------

ls
grep CDS 28035.66.PATRIC.gff | less  ### "global regular expression print" : recherche (et retrourne) toutes les lignes contenant le motif "CDS"

grep CDS 28035.66.PATRIC.gff | wc -l ### regarde combien il y en a 

grep CDS *.* | wc -l ### a fait le comptage sur tous les fichiers 


# Exemple 1 filtration : Récupérer les lignes correspondant aux CDS de protein-coding genes ? 

grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep CDS | less ### ne fait pas ce qu'on veut : trouve aussi des annotation CCDS

grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -w CDS | less ### ne recherche que les mots entiers => OK
grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -w CDS | wc -l ### voici leur nombre 

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l  ### en fait seul les protein-coding genes contiennent des CDS !

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf > gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.CDS.gtf

# Exemple 2 filtration : Récupérer les lignes des CDS correspondant au 1er exon ?

grep CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | less    ### filtrer pour les CDS : ne fait pas ce qu'on veut : trouve aussi des annotation CCDS,...
grep CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | less ### ne recherche que les mots entiers => OK
grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep exon_number 1 | less ### ne marche pas car l'espace entre exon_number et 1 est compris comme un séparateur de mot

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep 'exon_number 1' | less ### marche, mais ne fait pas ce qu'on veut : trouve aussi les exons 10, 11, 12, ...
grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep 'exon_number 1' | wc -l

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -w 'exon_number 1' | less => OK 
grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -w 'exon_number 1' | wc -l

grep -w CDS gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -w 'bexon_number 1' > gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.CDS_exon1.gtf

# Récupérer uniquement les lignes correspondant aux genes et aux transcrits de protein-coding genes

grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l ### OK : compte les lignes correspondant aux protein_coding genes

grep gene|transcript gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l ### utilise des expression régulières => ne marche pas car il faut protéger la regex (pense à un pipe !)
grep 'gene|transcript' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l ### ne marche pas car la version de base doit être "echappée" (interprétation littérale du texte)
grep 'gene\|transcript' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l ### marche mais peu lisible => utiliser plutôt -E (extended) ou encore mieux -P (perl)
grep -P 'gene|transcript' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l ### OK, mais en trouve trop car il faut un mot isolé
grep -P '\b(gene|transcript)\b' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | less ### OK (\b = bordure de mot),
grep -P '\b(gene|transcript)\b' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf |  wc -l ### , ou bien
grep -Pw 'gene|transcript' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | less ### OK 
grep -Pw 'gene|transcript' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | wc -l 

grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -Pw 'gene|transcript' | wc -l 
grep 'gene_type "protein_coding"' gencode.v35.chr20-1-10000_patch_hapl_scaff.basic.annotation.gtf | grep -Pw 'gene|transcript' > gencode.v35.chr20-22_patch_hapl_scaff.basic.annotation.CDS_gene_transcript.gtf

