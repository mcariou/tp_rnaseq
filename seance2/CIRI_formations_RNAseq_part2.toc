\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Objectif de la s\IeC {\'e}ance}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Qu'est-ce qu'un pipeline?}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Le PSMN}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}sentation du PSMN}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Connexion au PSMN}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Environnement de travail}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Quick start}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}O\IeC {\`u} sont les scripts?}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Fichier de param\IeC {\`e}trage}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Introduction g\IeC {\'e}n\IeC {\'e}rale}{5}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Param\IeC {\`e}tres g\IeC {\'e}n\IeC {\'e}raux}{5}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Param\IeC {\`e}tres des programmes}{6}{subsubsection.3.2.3}
\contentsline {subsection}{\numberline {3.3}Lancement en interactif sur un jeu de donn\IeC {\'e}es r\IeC {\'e}duit}{6}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Organisation des fichiers de sortie}{7}{subsection.3.4}
\contentsline {section}{\numberline {4}Donn\IeC {\'e}es et pipeline complet}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Donn\IeC {\'e}es brutes et espace partag\IeC {\'e}}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Fichier de param\IeC {\`e}tres}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Test du fichier}{10}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Lancement du pipeline complet sur le cluster}{10}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}R\IeC {\'e}sultats}{10}{subsection.4.5}
