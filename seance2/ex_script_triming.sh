#!/bin/sh


### Trimmomatic
data="/home/rstudio/data/mydatalocal/FASTQ"
out="/home/rstudio/data/mydatalocal/Trimmomatic"

mkdir -p $out/RNAseq_clean

for file in FL_wt_1 FL_wt_2 FL_ko_1 FL_ko_2 T3_wt_1 T3_wt_2 T3_ko_1 T3_ko_2
do

java -jar /softwares/Trimmomatic-0.39/trimmomatic-0.39.jar SE -phred33 ./RNASeq/${file}.fastq \
RNAseq_clean/${file}_clean.fastq LEADING:28 TRAILING:28 SLIDINGWINDOW:4:28 MINLEN:36

done

### fastqc

mkdir -p $out/fastqc_clean

fastqc $out/RNAseq_clean/* -o fastqc_cleanmkdir -p $out/fastqc_clean

