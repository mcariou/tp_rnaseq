\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{titling}
\usepackage{listings}
\usepackage{upquote}

\usepackage{xcolor}

\lstnewenvironment{code}[1][]{
	\lstset{
%		upquote=true,
		columns=flexible,
		basicstyle=\ttfamily,
		language=[LaTeX]TeX,
		texcsstyle=*\color{blue},
		commentstyle=\color{gray},
		frame=single,
		rulecolor=\color{green!5},
		backgroundcolor=\color{green!5},
	}
}{}

\lstnewenvironment{rep}[1][]{
	\lstset{
%		numbers=left,
%		upquote=true,
		columns=flexible,
		basicstyle=\ttfamily,
		language=[LaTeX]TeX,
		texcsstyle=*\color{blue},
		commentstyle=\color{gray},
		frame=single,
		%rulecolor=\color{green},
		backgroundcolor=\color{green!5},
%		xleftmargin=0em,% espace a gauche
%		xrightmargin=0em, % espace a droite
	}
}{}

\usepackage{geometry}
\geometry{hmargin=2.5cm,vmargin=1.5cm}

\usepackage[nottoc, notlof, notlot]{tocbibind}

\usepackage{hyperref}

\setlength{\droptitle}{-7em}
\title{RNAseq: Des reads bruts aux tableaux de comptage}
\author{Formation bioinformatiques du CIRI}
\begin{document}

\maketitle

Marie Cariou et Antoine Corbin

\tableofcontents

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Préambule}

\subsection{Conventions de présentation} 

Au cours de ce TP, nous allons effectuer une analyse de données de RNAseq "classique". Celle-ci comprend plusieurs étapes pour lesquelles nous allons utiliser différents programmes qui seront appelés en lignes de commande. Une introduction aux commandes unix de base a fait l'objet d'une séance préliminaire.

~

Dans ce document, les codes qui doivent être exécutés dans le terminal Linux sont donnés dans des encadrés de couleur vert clair dans le document PDF, comme ci-dessous :

\begin{code}
cd ~
ls
\end{code}

Les résultats des commandes exécutées dans le terminal seront donnés (si nécessaire) dans des encadrés vert clair, avec bordure, comme ci-dessous :

\begin{rep}
data
\end{rep}

~

Il est possible de copier les commandes présentées dans le document PDF et les coller dans le terminal.  

~

Il est recommandé de créer un fichier texte (un script), pour noter toutes les lignes de commande utilisées, avec des commentaires.


\subsection{Espace de travail} 

Les programmes utilisés dans ce TP doivent être installés. Pour simplifier, nous allons travailler sur des machines virtuelles. Il s'agit d'un environnement linux, configurées spécialement pour ce TP. Ces machines sont accessibles pour tous les utilisateurs, sur le cloud de l’Institut Français de Bioinformatique (\url{https://biosphere.france-bioinformatique.fr/catalogue/}). Tous les outils nécessaires ont déjà été installés.

~

Accédez à une machine virtuelle selon les instructions données lors de la première séance.

Vous pouvez tester quelques commandes de bases:

\begin{code}
pwd
ls
\end{code}

\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

\subsection{Contexte de l'étude}

Nous allons réanalyser des données tirées d’une publication qui s’est intéressée aux fonctions d'un long ARN non-codant, appelé Hotair, chez la souris. Ce gène serait impliqué chez les vértebrés dans la régulation des gènes HOX, qui codent pour des protéines importantes pour le développement embryonnaire \cite{Rinn2007}.

~

Une première analyse transcriptomique, \textit{in vitro}, sur des cultures cellulaires a révélé que des souris “knock-out”, comportant une délétion de ce locus, présentaient une augmentation du niveau d’expression des gènes HOXD, par rapport aux cellules de souris sauvages \cite{Li2013}.
Pour vérifier si ces conclusions étaient valables aussi \textit{in vivo}, un autre groupe de chercheurs a analysé le transcriptome des souris sauvages et des souris mutantes dans des tissus embryonnaires \cite{10.1371/journal.pgen.1006232}. 
Plusieurs tissus ont été analysés : les membres supérieurs (FL) et inférieurs (HL), le tubercule génital (GT), ainsi que trois régions du tronc (T1, T2 et T3). Pour chaque tissu, on a généré des données de RNAseq pour deux individus sauvages (wt) et deux mutants (ko). 

~

Les données ont été générées avec le protocole Illumina TruSeq, single-end, avec sélection polyA, brin-spécifique.
Ces données sont accessibles dans la base de données NCBI SRA avec le numéro d’accession SRP071333 \url{https://www.ncbi.nlm.nih.gov/sra?term=SRP071333}. 
Au cours de cette séance, nous allons analyser 2 replicas des échantillons générés pour les membres supérieurs (FL) et pour le segment postérieur du tronc (T3) de souris sauvages (WT) et mutantes (KO) (8 échantillons).

~

Nous souhaitons ainsi identifier les gènes cibles (directes et indirectes) de l’ARN non-codant Hotair, en comparant les transcriptomes des souris sauvages et mutantes (analyse d'expression différentielle).

~

La figure 1 représente la suite d'analyse que nous allons réaliser.

\begin{figure}[h!]
    \center
    \includegraphics[width=9cm]{../figure/pipeline.png}
    \caption{Pipeline d'analyse RNAseq. Les fichiers d'entrée et sorties de chaque étape sont indiqués par des cadres bleues. Les noms des programmes utilisés pour chaque étape (cadre orange) sont indiqués en vert}
    \label{pip}
\end{figure}

\subsection{Données fournies}

Les données que vous utiliserez au cours de ce TP sont fournies dans l’archive TP\_RNASeq\_genome.tar.gz.
Il contient un extrait des fichiers de lectures brutes initiaux, pour les 6 échantillons décrits au paragraphe précédent.

La plupart des programmes utilisés au cours de ce TP se lancent en ligne de commande de la façon suivante:

{\footnotesize
\begin{code}
nom_du_programme [-option1 valeur_option1 ... -optionN valeur_optionN] ... arguments

\end{code}
}

Par exemple, pour télécharger les données dans votre répertoire, vous pouvez utiliser la commande \textit{wget}, suivi par l'adresse de l'archive. Voici la commande :

\begin{code}

wget ftp://pbil.univ-lyon1.fr/pub/cours/formation_ngs/day4/2019/TP_RNASeq_genome.tar.gz

\end{code}

L'objet obtenu est un répertoire compressé, pour accéder aux fichiers qu'il contient, il faut le désarchiver. On utilise pour cela la commande \textit{tar} avec les options "-xzvf", qui servent à préciser l'action a effectuer et le type d'archive à traiter.

Désarchivez le répertoire et placez-vous dedans :

\begin{code}

tar -xzvf TP_RNASeq_genome.tar.gz
cd TP_RNASeq_genome
ls
\end{code}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Description des fichiers de données}

Le répertoire \textbf{TP\_RNASeq\_genome} contient lui-même 4 (sous-)répertoires. Affichons le contenu du sou-repertoire \textbf{RNAseq}:

\begin{code}
ls
ls RNASeq/
\end{code}
{\footnotesize
\begin{rep}
FL_ko_1.fastq  FL_ko_2.fastq  FL_wt_1.fastq  FL_wt_2.fastq  T3_ko_1.fastq  T3_ko_2.fastq  T3_wt_1.fastq  T3_wt_2.fastq
\end{rep}
}
Ces 8 fichiers sont des fichiers de séquences, au format fastq. Ils ne contiennent qu’un petit sous-échantillon des lectures disponibles, ce qui nous permettra d’effectuer rapidement toute l’analyse au cours de cette séance.

~

Affichons les premières lignes pour savoir à quoi ressemble ce format:

\begin{code}
head RNASeq/FL_ko_1.fastq
\end{code}
{\footnotesize
\begin{rep}
@SRR3213050.8665
TGCTTGCAAGTGTAGTAGGGAGAGAGATTTGGACCCAACAGTTTGCTTGTGTCTCTGCACAATCCTGTAAACAAACTTGAAGCCACGTGCTATTGGGAGA
+
???DDDDDDDD2AEEEE4FEEAFFCF;EE>EIEIIIDDDDIECCEIEEE?B9?CDEIIEIIIIIDECCCEDDDDD;?ADDDAAAAAA>???AEAAA???>
@SRR3213050.9144
GCTTCGTTATCCGAGGTGTCTGGACTGGAGTCTGCGGTCTTGGCCCGTTCCTTCTCGCTCTCCGAGGGGCTGGCTTTTGGGCCCGCCAGGCTTTGCTCTG
+
CCCFFFFBHHGHH<FG@CHGGGHEGIGHIICDBHGHIFFHGGHGBDA;C@DHFEEFHFFDCECC?BC=;;9@BB9+9A@BBBBBB3<>>?B89?C:@4>:
@SRR3213050.29082
GTGTGAGGGGGGCACGGAGCACAGGGTGGGGGCAGGGGTGTAAGTTATGTCTTATAACCTGGTGATGTCCTCTGCCCGTTGCTGCTCCGGCGGCGTGGCG

\end{rep}
}

Chaque groupe de 4 lignes représente 1 lecture (c'est-à-dire une séquence obtenue par séquençage haut débit, un \textit{read}):
\begin{itemize}
	\item la première contient le nom de la lecture (introduit par le caractère @)
	\item la deuxième contient la séquence en elle-même (suite de T, C, A et G)
	\item la troisième contient un "+"
	\item la quatrième contient un code indiquant le score de qualité associé à chaque base.

	Ce code est relié au score de qualité Phred qui est lui-même relié à la probabilité d'erreur de la façon suivante:

	$q=-10*log_{10}(P)$ (un score de 10 correspond à une précision de 90\%, un score de 20, de 99\% etc) 
\end{itemize}	

%\textit{Décrire le score de qualité?}
~

On peut calculer le nombre de lectures dans chaque fichier, soit en utilisant la commande wc, qui, avec l'option -l permet d'afficher le nombre de ligne dans un fichier.

\begin{code}
wc -l RNASeq/*.fastq
\end{code}
{\footnotesize
\begin{rep}
6980	RNASeq/FL_ko_1.fastq
13648	RNASeq/FL_ko_2.fastq
9432	RNASeq/FL_wt_1.fastq
7324	RNASeq/FL_wt_2.fastq
81680	RNASeq/T3_ko_1.fastq
96268	RNASeq/T3_ko_2.fastq
113120	RNASeq/T3_wt_1.fastq
102856	RNASeq/T3_wt_2.fastq
431308	total
\end{rep}
}

Soit avec la commande grep qui permet de rechercher toutes les occurence d'un motif (ici par exemple le motif commun aux noms de reads), avec l'option "-c" qui permet d'afficher le nombre de ligne contenant le motif recherché.

~

Le "motif" recherché est une expression régulière.

\begin{code}
grep -c "^@SRR" RNASeq/*.fastq
\end{code}
{\footnotesize
\begin{rep}
TP_RNASeq_genome/RNASeq/FL_ko_1.fastq:1745
TP_RNASeq_genome/RNASeq/FL_ko_2.fastq:3412
TP_RNASeq_genome/RNASeq/FL_wt_1.fastq:2358
TP_RNASeq_genome/RNASeq/FL_wt_2.fastq:1831
TP_RNASeq_genome/RNASeq/T3_ko_1.fastq:20420
TP_RNASeq_genome/RNASeq/T3_ko_2.fastq:24067
TP_RNASeq_genome/RNASeq/T3_wt_1.fastq:28280
TP_RNASeq_genome/RNASeq/T3_wt_2.fastq:25714
\end{rep}
}

Le fichier FL\_ko\_1 contient 1745 séquences, chaque séquence étant écrite sur 4 lignes, le fichier devrait contenir 1745x4= 6980 lignes. C'est bien le cas.

\section{Contrôles qualités, \textit{fastQC}}

Pour vérifier la qualité d'un jeu de données brut, il existe des programmes permettant de décrire les caractéristiques d'une fichier de séquences, notamment la qualité des séquences.
Nous allons pour cela utiliser le programme FastQC version 0.11.2 (\url{http://www.bioinformatics.babraham.ac.uk/projects/fastqc/})

~

Pour la plupart des programmes, vous pouvez obtenir une brève description ainsi que la liste des paramètres disponibles en tapant le nom du programme et l'option \textit{"--help"}. Par exemple:

\begin{code}
fastqc --help

\end{code}

Le seul argument obligatoire est le (ou les) noms de fichiers de reads à analyser, l'expressions"./RNASeq/*.fastq" (où le caractère "*" signifie "n'importe quel caractère") permet de traiter en une fois tous les fichiers fastq contenus dans le répertoire RNAseq.

Nous pouvons également utiliser comme paramètre optionnel -o, le nom du répertoire où seront écrits les fichiers de sortie: "-o fastqc". Ce répertoire doit être créé au préalable (\textit{mkdir fastqc}).

\begin{code}
mkdir fastqc
fastqc ./RNASeq/*.fastq -o fastqc
\end{code}

La commande devrait avoir écrit 1 fichier HTML et un repertoire compressé '.zip' pour chaque fichier de read.

\begin{code}
ls -lh fastqc/
\end{code}

{\footnotesize
\begin{rep}
FL_ko_1_fastqc.html  FL_ko_2_fastqc.html  FL_wt_1_fastqc.html  FL_wt_2_fastqc.html  
T3_ko_1_fastqc.html  T3_ko_2_fastqc.html  T3_wt_1_fastqc.html  T3_wt_2_fastqc.html
FL_ko_1_fastqc.zip   FL_ko_2_fastqc.zip   FL_wt_1_fastqc.zip   FL_wt_2_fastqc.zip   
T3_ko_1_fastqc.zip   T3_ko_2_fastqc.zip   T3_wt_1_fastqc.zip   T3_wt_2_fastqc.zip
\end{rep}
}

%Pour parcourir un de ces fichiers de sortie, il faut le recopier sur votre machine (en local), depuis la machine virtuelle.
%Ouvrez un nouveau terminal, vous vous trouvez ainsi dans un répertoire de travail situé sur votre propre machine (et non plus sur la machine virtuelle). La commande scp permet de copier un fichier depuis un serveur distant. La syntaxe est la suivante:

~

%\textit{scp source destination}

%\begin{code}
%# a executer en local!
%scp stage[1-5]@adresse_IP:TP_RNASeq_genome/fastqc/FL_ko_1_fastqc.html .
%\end{code}

Vous pouvez ouvrir un ou plusieurs de ces fichiers html dans une interface web en cliquant dessus dans l'onglet "file". Observons en particulier la distribution de la qualité par base (1er graphique, figure \ref{fastqc}). 
L'axe des ordonnés représente le score de qualité Phred évoqué plus haut, la zone vert correspond donc à une précision supérieure à 99,8\% et la zone orange à une précision comprise entre 99 et 99,8\%).

~

On voit que la qualité diminue à la fin de la séquence, toutefois, ici, elle reste élevée (dans la zone "verte") sur toute la longueure des lectures. Ce résultat correspond à un séquençage de bonne qualité.

\begin{figure}[h!]
    \center
    \includegraphics[width=12cm]{../figure/per_base_quality.png}
    \caption{Pair base sequence quality distribution}
    \label{fastqc}
\end{figure}

\section{Trimming}

~

Bien que nous ayons vu que ces données sont de bonnes qualités, on peut vouloir effectuer une étape de nettoyage supplémentaire, afin de n'utiliser que des lectures de très très bonne qualité.
Nous allons pour cela utiliser le programme \textit{Trimmomatic}, donc la documentation se trouve à cette adresse \url{http://www.usadellab.org/cms/?page=trimmomatic}.

~

Ce programme peut également être utilisé pour supprimer les adaptateurs utilisés pour le séquençage le cas échéant. Ici, ce n'est pas nécessaire.

~

\textbf{Pour 1 échantillon}

~

\begin{code}
mkdir RNAseq_clean

java -jar /softwares/Trimmomatic-0.39/trimmomatic-0.39.jar SE -phred33 ./RNASeq/FL_wt_1.fastq \
RNAseq_clean/FL_wt_1_clean.fastq LEADING:28 TRAILING:28 SLIDINGWINDOW:4:28 MINLEN:36

\end{code}

Cette commande permet de: 
\begin{itemize}
	\item \textit{LEADING:28} Supprimer les bases en dessous d'en seuil de qualité (28), ou bien les N, en DEBUT de séquence.
	\item \textit{TRAILING:28} Supprimer les bases en dessous d'en seuil de qualité (28), ou bien les N, en FIN de séquence.
	\item \textit{SLIDINGWINDOW:4:20} parcourir les reads avec une fenètre glissante de 4-base, et couper quand la qualité moyenne descend en dessous de 20.
	\item \textit{MINLEN:36} supprimer les reads de moins de 36 paires de base.
\end{itemize}

~

\textbf{Pout tout les échantillons (boucle)}

~

Pour effectuer le nettoyage sur tout les échantillons, nous avons deux options : soit nous remplaçons “manuellement” T3\_wt\_1 par les autres noms d’échantillons dans la commande au-dessus, soit nous utilisons une boucle. 

~

Voici le principe de construction d'une boucle for:

~

\textbf{for} variable \textbf{in} liste-de-valeur

\textbf{do}

instruction

\textbf{done}

~

Les \textit{instructions} seront ainsi appliquées à chaque élément contenu dans \textit{liste-de-valeur}.

Ces "éléments" constituent des variables. Dans le bloc \textit{instruction}, "\${variable}" prendra successivement pour valeur les éléments contenu dans \textit{liste-de-valeur}. 

~

Dans notre cas :

\begin{code}
for file in FL_wt_2 FL_ko_1 FL_ko_2 T3_wt_1 T3_wt_2 T3_ko_1 T3_ko_2 ## FL_wt_1 deja fait
do
java -jar /softwares/Trimmomatic-0.39/trimmomatic-0.39.jar SE -phred33 ./RNASeq/${file}.fastq \
RNAseq_clean/${file}_clean.fastq LEADING:28 TRAILING:28 SLIDINGWINDOW:4:28 MINLEN:36
done
\end{code}

Utilisez un éditeur de texte pour écrire (et corriger au besoin) votre commande!

~

\textbf{Résultats:}

~

Comptons de nombre de lectures à l'issu de ce nettoyage.

\begin{code}
grep -c "^@SRR" RNAseq_clean/*.fastq
\end{code}

\begin{rep}
RNAseq_clean/FL_ko_1_clean.fastq:1454
RNAseq_clean/FL_ko_2_clean.fastq:2871
RNAseq_clean/FL_wt_1_clean.fastq:1957
RNAseq_clean/FL_wt_2_clean.fastq:1470
RNAseq_clean/T3_ko_1_clean.fastq:16824
RNAseq_clean/T3_ko_2_clean.fastq:19869
RNAseq_clean/T3_wt_1_clean.fastq:22056
RNAseq_clean/T3_wt_2_clean.fastq:21071
\end{rep}

Vous pouvez également utiliser de nouveau fastQC sur les fichiers nettoyés afin de visualiser l'effet du trimming.

\begin{code}
mkdir fastqc_clean

fastqc ./RNAseq_clean/*.fastq -o fastqc_clean
\end{code}



\section{Mapping et quantification}

\subsection{Génome et annotation}

\subsubsection{Format des fichiers}

Le dossier \textbf{genome} contient un seul fichier (genome.fa), qui contient une partie du génome de la souris au format fasta. Pour réduire le temps d’exécution des commandes, ici nous allons analyser une petite région du génome de la souris, comprenant environ 2Mb sur deux chromosomes différents (2 et 15).

~

Visualisons les 1eres lignes de ce fichier:

\begin{code}
head genome/genome.fa
\end{code}
{\footnotesize
\begin{rep}
>15
GTGGAAAATACTCAAGAAATAGCTAGGAATATATGGGTGTTGCTCTTGATCTCACATGGG
CATCAGGCATGCAGATGTGCACTCTTTCTAGGCTTTACTGACAACAACTTGATGGACCAT
GGAGTCTGGGGAAGAAAATGAGCTGAGGTAGTATCCAGAGACAACAGACATGTACCTGCT
ATCTATGTACAAGACACACTCCAGCATGCATTTGTCACAAAAGAGCTCAGCTAAGAACTT
GTTTCCAAGCCAGTCTTTACTGAATTTGACAATGTCCCTTTATCCAGAAGCACCGTGTGA

\end{rep}
}

~

Le dossier \textbf{annot} contient un fichier au format GTF (annot.gtf), extrait de la base de données Ensembl 93 (\url{http://www.ensembl.org}). 

~

Le format GTF (General Transfer Format) permet de présenter des annotations (d'exons, de transcrits, de genes, CDS etc.). 
Chaque ligne correspond à une annotation sur une région génomique. Les colonnes, séparées par des tabulations, sont les suivantes :

~
\begin{itemize}
\item chromosome
\item origine de l’annotation (par exemple la base de données Ensembl)
\item type de région annotée (par exemple le gène, exon, CDS etc.)
\item coordonnée de début
\item coordonnée de fin
\item score (une valeur numérique, qui peut représenter la qualité de l’annotation ; les valeurs manquantes sont représentées par un point .)
\item brin ("+", "-" ou "." pour les éléments non orientés)
\item cadre de lecture (0, 1, 2 pour le CDS, . pour les autres régions génomiques)
\item informations supplémentaires (plusieurs informations peuvent être données, séparées par des points-virgules).
\end{itemize}

\textit{Notons qu'il existe un format de fichier d'annotation similaire mais plus moderne appelé GFF3 (.gff ou .gff3)}

~

Visualisons les 2 premières lignes de ce fichier (-n 2):

\begin{code}
head -n 2 annot/annot.gtf
\end{code}

{\footnotesize
\begin{rep}
1	15 ensembl_havana gene 73359 81070 . + . 
	gene_id "ENSMUSG00000001655"; gene_version "6"; gene_name "Hoxc13"; gene_source "ensembl_havana"; 
	gene_biotype "protein_coding";
2	15 ensembl_havana transcript 73359 81070 . + . 
	gene_id "ENSMUSG00000001655"; gene_version "6"; transcript_id "ENSMUST00000001700"; 
	transcript_version "6"; gene_name "Hoxc13"; gene_source "ensembl_havana"; 
	gene_biotype "protein_coding"; transcript_name "Hoxc13-201"; transcript_source "ensembl_havana"; 
	transcript_biotype "protein_coding"; tag "CCDS"; ccds_id " CCDS27890"; tag "basic"; 
	transcript_support_level "1";
\end{rep}
}

\subsubsection{Principe de l'analyse}

L'analyse s'effectue en 2 étapes:
\begin{itemize}
	\item Alignement des lectures sur le génome avec un logiciel dédié (ici \textbf{HISAT2}, mais il existe aussi STAR, TopHat...) 
	\item Comptage du nombre de lectures attribuées à chaque gène et/ou chaque isoforme (ici \textbf{HTSeq-counts}, mais il existe aussi featureCounts...)
\end{itemize}

\begin{figure}[h!]
    \center
    \includegraphics[width=15cm, trim = 10cm 20cm 10cm 7cm, clip]{../figure/figcours1.png}
    \caption{Alignement des lectures de RNAseq sur un génome annoté. Les cadres verts représentent les exons, les lignes marrons les lectures alignées au sein des exons et les lignes bleus les lectures alignées épissées}
    \label{pip}
\end{figure}



~

Chaque lecture est attribuée à un gène en fonction du chevauchement de leur alignement et des coordonnées des exons des gènes. Cette attribution peut rencontrer différents types de difficultés:

\begin{itemize}
	\item alignements ambigus (ou multiples)
	\item chevauchement des gènes
\end{itemize}

Ces cas peuvent être traités de différentes façons:

\begin{itemize}
	\item comptage des lectures alignées de manière non-ambiguë (unique)
	\item attribution des lectures ambiguës proportionnellement aux nombres de lectures uniques
\end{itemize}

~

\textit{remarque 1: Il est important de connaître les coordonnés des introns et exons, notamment pour identifier les lectures à cheval sur des limites d'exons (lectures alignées épisées (en bleu))}

~

\textit{remarque 2: Pour les génomes sans introns (bactéries), il existe des outils sont plus simple (bowtie2, BWA, etc)} 

~

\textit{remarque 3: Il existe des outils comme kallisto qui realisent cette quantification en une seule étape grace à un pseudo-alignement sur le transcriptome}

\subsection{Alignement des données de RNAseq sur le génome}


Pour aligner les lectures de RNAseq sur le génome, nous allons utiliser le logiciel HISAT2 \cite{Kim2015}, version 2.1.0. 
\url{http://daehwankimlab.github.io/hisat2/}

\subsubsection{Construction de l’index génomique}

Avant l'alignement des lectures à proprement parler, HISAT2 construit un index. Il s'agit d'une représentation du génome optimisée pour la recherche d’alignements. La principale source d’information pour la construction de l’index est donc la séquence du génome. Pour permettre une recherche efficace pour les alignements épissés, HISAT2 utilise les annotations génomiques pour inclure les introns connus dans l’index. 

~

La construction de l’index se fait donc en deux étapes. Premièrement, il faut extraire les coordonnées des exons et des introns (ou sites d’épissage) à partir des annotations au format GTF, dans un format lisible par HISAT2. Cela se fait avec les scripts hisat2\_extract\_exons.py et hisat2\_extract\_splice\_sites.py, qui sont fournis avec le programme HISAT2.

\begin{code}
cd annot
python hisat2_extract_exons.py annot.gtf > exons.txt
python hisat2_extract_splice_sites.py annot.gtf > introns.txt
cd ..
ls annot/
\end{code}

Ouvrez les fichiers ainsi créés avec la commande head. Vous remarquerez que ces fichiers ont un format très simple : il s’agit de plusieurs colonnes séparées par des tabulations, contenant les coordonnées des exons et des introns, respectivement.
Ensuite, on peut construire l’index génomique à partir de ces deux fichiers et de la séquence du génome, avec la fonction hisat2-build.

\begin{code}
cd genome
hisat2-build -p 2 --ss ../annot/introns.txt --exon \
../annot/exons.txt genome.fa genome
ls
\end{code}

Nous avons utilisé le programme hisat2-build en précisant plusieurs paramètres :
\begin{itemize}
\item \textit{-p} nombre de processeurs (ou threads) utilisés pour le calcul ;
\item \textit{-ss} fichier contenant les coordonnées des introns, obtenu précédemment ;
\item \textit{-exon} fichier contenant les coordonnées des exons, obtenu précédemment ;
\item \textit{genome.fa}: fichier contenant la séquence du génome au format fasta ;
\item \textit{genome}: préfixe qui sera utilisé pour les fichiers de sortie.
\end{itemize}

~

Cette commande a créé 8 fichiers de sortie, qui s’appellent genome.1.ht2, genome.2.ht2, . . .genome.8.ht2. 

Vérifiez l’index construit en utilisant la commande hisat2-inspect :

\begin{code}

hisat2-inspect -s genome
\end{code}

Combien y a-t-il de chromosomes dans l’index obtenu ? Combien d’exons et d’introns ?



\subsubsection{Alignement des reads}

\textbf{Pour un échantillon}

Nous allons ensuite aligner les échantillons de RNAseq, avec le programme principal de HISAT2. Pour cela, nous allons créer le répertoire res\_hisat.

Dans un premier temps, nous allons faire cela pour l’échantillon T3\_wt\_1, en utilisant les commandes suivantes :

{\footnotesize
\begin{code}
cd ..
mkdir res_hisat
cd res_hisat
hisat2 -p 2 -x ../genome/genome -U ../RNAseq_clean/T3_wt_1_clean.fastq -S T3_wt_1_aln.sam --rna-strandness R --no-unal

\end{code}
}

Nous avons utilisé la commande hisat2 en précisant plusieurs paramètres :
\begin{itemize}
\item \textit{-p} correspond au nombre de processeurs utilisés pour le calcul (ici 2 processeurs) ;
\item \textit{-x} chemin d’accès pour l’index génomique créé précédemment ;
\item \textit{-U} chemin d’accès pour le fichier contenant les données de RNAseq. 
\item \textit{-S} chemin du fichier de sortie, au format SAM (qui sera expliqué plus loin).
\item \textit{-rna-strandness} pour préciser que les données de RNAseq sont orientées (strand-spécifique). Dans ce cas-ci, les données sont de type “R”, ce qui signifie que l’on séquence le brin complémentaire à l’ARNm.
\item \textit{-no-unal} sert à préciser que nous ne souhaitons pas inclure les lectures non-alignées dans le fichier de sortie.
\end{itemize}

~

\textbf{Pour tout les échantillons (boucle)}


\begin{code}
cd res_hisat
for file in FL_wt_1 FL_wt_2 FL_ko_1 FL_ko_2 T3_wt_2 T3_ko_1 T3_ko_2 ## T3_wt_1 deja fait
do
hisat2 -p 2 -x ../genome/genome -U ../RNAseq_clean/${file}_clean.fastq -S ${file}_aln.sam \
--rna-strandness R --no-unal
done
cd ..
\end{code}



~

Cette étape est extrêmement rapide, d’une part grâce à la performance du logiciel utilisé, mais aussi car nos échantillons sont très réduits - seulement quelques milliers de lectures. Pour des échantillons de RNAseq complets, contenant plusieurs millions de lectures, on a des temps de calcul de l’ordre de l’heure.

\subsubsection{Visualisation des alignements}

\textbf{Visualisation des fichiers Sam}

Les fichiers au format SAM peuvent être ouverts directements en ligne de commande, par exemple avec la commande head.

\begin{code}
head res_hisat/FL_wt_1_aln.sam
\end{code}
{\footnotesize
\begin{rep}
1	@HD	VN:1.0	SO:unsorted
2	@SQ	SN:15	LN:275981
3	@SQ	SN:2	LN:896074
4	@PG	ID:hisat2 PN:hisat2 VN:2.2.0 CL:"/home/adminmarie/programmes/hisat2-2.2.0/hisat2-align-s 
	--wrapper basic-0 -p 2 -x ../genome/genome --rna-strandness R --passthrough --read-lengths 100 -U 
	../RNASeq/FL_wt_1.fastq"
5	SRR3213056.97467	16	15	168975	60	100M	*	0	0	ACATAGAAGAAAGGTTGAGTCCTCT
	GTCTAGATGCCAGAAGGATTCGCAGCAATAGTAGGTGCTGCTTGGTAAAGAGGTGTTGGGGAAGGAGAAATGGCC	DEEDDDDCAAA?A<>5ADDBBCCC>FEF
	FFFGHHGHJJJJIHGGBHGGHECIHGGHGIJIJJIIHF@IHGGGJJIIJJJJJJJJJJIFHHHHFFFFFC@@	AS:i:0XN:i:0	XM:i:0	
	XO:i:0	XG:i:0	NM:i:0	MD:Z:100	YT:Z:UU	XS:A:+	NH:i:1
6	SRR3213056.149375	16	15	186982	60	100M	*	0	0	ATCATGAGCTCGTATTTGATGGACT
	CTAACTACATCGATCCGAAATTTCCTCCATGCGAAGAATATTCGCAAAATAGCTACATCCCTGAACACAGTCCGG	DDDDCDBDDBDDCDDDDDEDDCCADEEE
	EEEECDFFHHHJJJJJJJJJGGJJIJJJJJJJJJJJJJJJJIJJJJIJIIHJIGJJIIHFHHHFFFFFCCC	AS:i:0XN:i:0	XM:i:0	
	XO:i:0	XG:i:0	NM:i:0	MD:Z:100	YT:Z:UU	XS:A:+	NH:i:1
7	SRR3213056.109764	16	15	90792	60	98M2S	*	0	0	TAGCCCCAGACTAAGCTTGCCTTGG
	TGGAGCAAGAGGGGGCGCAGCCTGGGACATAGTCCCGCTTACAACGCCAGGCCTTCTGGCAGGCCCTTCCCGGCT	BDBBDDDCCCCAAAB??BDDBBB?<DDD
	CDC>DBBBDB@BD@?BA?AEEB>>FCCDDBJIIGJGIIGHFIIGJIGHEGJIIIHBIGIFFFBHAFFFF@@@	AS:i:-2XN:i:0	XM:i:0	
	XO:i:0	XG:i:0	NM:i:0	MD:Z:98	YT:Z:UU	XS:A:+	NH:i:1
8	SRR3213056.194697	16	15	163032	60	100M	*	0	0	ACTCTCGGTACCAGACCCTGGAACT
	GGAGAAGGAATTTCACTTCAACCGCTACCTAACTCGGCGCCGGCGCATCGAGATCGCCAATGCTCTGTGCCTGAC	@DDDDDDDDDDDDDDDDDEDDDDCADDD
	DDDDDDDDEDDCDDDDDDDDDDDCDDDDDDDFHHJJJJJJJJJJJJJJJJJJIJJJJJJHHHHHFFFFFCCC	AS:i:0XN:i:0	XM:i:0	
	XO:i:0	XG:i:0	NM:i:0	MD:Z:100	YT:Z:UU	XS:A:+	NH:i:1

\end{rep}
}

Les 4 premières lignes, qui commencent par "@", constituent un \textit{header}. Celui-ci fournit des informations générales sur le contenu du fichier. Par exemple, on retrouve le nom des 2 chromosomes sur lesquels l'alignement a été effectué (2 et 15).

Les lignes suivantes sont des lignes d'un tableau, où chaque ligne correspond à l'alignement d'un read (nom, position...).  Les champs sont décrits ici: 
\url{http://www.htslib.org/doc/sam.html}

~

Nous pouvons ainsi déjà voir, grace au "header", que les résultats de HISAT2 ne sont pas triés par rapport au chromosome et à la position du début (@HD VN:1.0 SO:unsorted). Pour que les programmes d’analyse et de visualisation des alignements fonctionnent de manière efficace, les alignements doivent être triés. Nous allons faire cela avec le programme \textit{samtools sort}, issu de le suite de programme SAMtools \url{http://www.htslib.org/}. En plus de trier les fichiers, nous allons aussi créer des indexes, ce qui permettra de mieux les analyser et visualiser ensuite.


~

\begin{code}
cd res_hisat
for file in FL_wt_1 FL_wt_2 FL_ko_1 FL_ko_2 T3_wt_1 T3_wt_2 T3_ko_1 T3_ko_2
do
samtools sort -m 1G -@ 1 -o ${file}_sorted_aln.bam -O bam ${file}_aln.sam
samtools index ${file}_sorted_aln.bam
done
cd ..
\end{code}

Les fichiers de sortie de la procédure de tri sont au format BAM, qui est la variante binaire de SAM. Pour les visualiser, il faut cette fois-ci utiliser le programme samtools.

\begin{code}
samtools view res_hisat/T3_wt_1_sorted_aln.bam | head
\end{code}

\textbf{Avec un navigateur de génome}
 
~

Nous allons utiliser le logiciel IGV, qui est un navigateur de génome. Il permet de parcourir chaque chromosome, en affichant plusieurs types de données, y compris les annotations génomiques et les sorties de HISAT2.

Ce programme ne peut pas être lancé depuis l'interface Rstudio des machines virtuelles de l'IFB. Il est donc nécessaire de télécharger et d'installer ce programme sur votre machine.

~

Il sera également nécessaire de recopier en local les fichiers utiles. Vous pouvez pour cela utiliser la commande "export", pour exporter, la séquence génomique de référence, et les fichiers sorted\_aln.bam pour les échantillons T3\_wt\_1 et T3\_ko\_1. 

~

\textbf{!! En local !! }

~

Télécharger la version de IGV appropriée pour votre ordinateur parmi les versions proposées sur ce site:

\url{http://software.broadinstitute.org/software/igv/download}

~

Lancer le programme.

~

Nous allons ensuite charger la séquence génomique de référence dans IGV. Allez dans le menu Genomes, Load genome from file, et sélectionnez le fichier TP\_RNASeq\_genome/genome/genome.fa.

Ensuite, pour charger les annotations, utilisez le menu File, Load from file, et sélectionnez le fichier  TP\_RNASeq\_genome/annot/annot.gtf. Cela permet de visualiser les gènes présents sur ce chromosome. Il est possible de faire des recherches par mot-clef. Par exemple, il est possible de chercher le gène Hotair.

Pour charger les alignements obtenus avec HISAT2, utilisez toujours le menu File, Load from file et chargez les fichiers sorted\_aln.bam pour les échantillons T3\_wt\_1 et T3\_ko\_1. 

Vous pouvez renommer les données en faisant clic droit sur le nom initialement affiché dans la colonne de gauche, Rename track. Que remarquez-vous dans la région correspondant au gène Hotair?



\subsection{Comptages des nombres de lectures par gène}

Les alignements obtenus avec HISAT2 nous permettent également d’estimer les niveaux d’expression des gènes de manière quantitative. Nous allons pour cela comptabiliser le nombre de lectures qui s’alignent de manière non-ambiguë sur chaque gène avec le package Python HTSeq \cite{10.1093/bioinformatics/btu638}. Le manuel de cette librairie se trouve à l’adresse : \url{https://htseq.readthedocs.io/en/release\_0.10.0/}.

\begin{code}
mkdir comptages
cd comptages
for file in FL_wt_1 FL_wt_2 FL_ko_1 FL_ko_2 T3_wt_1 T3_wt_2 T3_ko_1 T3_ko_2
do
htseq-count -f bam -m union -s reverse -t gene ../res_hisat/${file}_sorted_aln.bam \
../annot/annot.gtf > ${file}.txt
done
cd ..
\end{code}

Nous avons précisé les options suivantes :
\begin{itemize}
\item \textit{-f bam}: le format du fichier d’entrée pour les alignements ;
\item \textit{-m union}: le mode de calcul pour les lectures qui chevauchent avec plusieurs gènes ;
(voir \url{https://htseq.readthedocs.io/en/master/count.html})
\item \textit{-s reverse} le type de librairie RNAseq brin-spécifique, ce qui correspond aux librairies TruSeq stranded mRNA - on séquence le brin complémentaire à l’ARNm ;
\item \textit{-t gene} l’unité sur laquelle nous voulons comptabiliser les lectures, ici le gène ;
\item le fichier contenant les lectures alignées avec HISAT2 ;
\item le fichier contenant l’annotation du génome.
\end{itemize}

~

La sortie est redirigée vers un fichier txt, par exemple comptages/FL\_ko\_1.txt Le format des fichiers de sortie est très simple. Il y a deux colonnes, l’une contenant l’identifiant du gène et l’autre le nombre de lectures qui lui sont attribuées. Si l’on ouvre un des fichiers de sortie avec la commande "less comptages/FL\_wt\_1.txt", on se rend compte que la plupart des gènes ont des valeurs de comptages égales à 0. Pas d’inquiétude, c’est normal car nous avons largement sous-échantillonné les données de RNAseq pour ne couvrir qu’une petite partie du chromosome. Nous pouvons à nouveau vérifier les valeurs obtenues pour Hotair, dont l’identifiant dans la base de données Ensembl est ENSMUSG00000086903.

\begin{code}
grep ENSMUSG00000086903 comptages/*txt
\end{code}

\begin{rep}
comptages/FL_ko_1.txt:ENSMUSG00000086903	0
comptages/FL_ko_2.txt:ENSMUSG00000086903	0
comptages/FL_wt_1.txt:ENSMUSG00000086903	2
comptages/FL_wt_2.txt:ENSMUSG00000086903	4
comptages/T3_ko_1.txt:ENSMUSG00000086903	0
comptages/T3_ko_2.txt:ENSMUSG00000086903	0
comptages/T3_wt_1.txt:ENSMUSG00000086903	357
comptages/T3_wt_2.txt:ENSMUSG00000086903	289
\end{rep}

Nous pouvons déjà nous rendre compte que Hotair n’est pas détecté dans les échantillons mutants, ce qui est attendu et rassurant. Nous remarquons également que le niveau d’expression de ce gène paraît plus grand dans la partie postérieure du tronc (T3) que dans les membres (FL). Pour quantifier ces impressions, nous devons comparer des niveaux d’expression normalisés, ainsi que faire une analyse d’expression différentielle.

\section{Conclusion}

Vous pouvez récupérer l'ensemble des résultats obtenus pendant la séance en copiant le répertoire sur votre machine, grace à la fonction "export".
%Ouvrez un nouveau terminal, vous vous trouvez ainsi dans un répertoire de travail situé sur votre propre machine (et non plus sur la machine virtuelle). La commande scp introduite précédemment permet de copier un répertoire depuis un serveur distant, grace l'option \textit{- r},. 

%\textit{scp -r source destination}

%\begin{code}
%# a executer en local!
%scp -r stage[1-5]@adresse_IP:TP_RNASeq_genome .
%\end{code}

~

Rendez-vous aux prochaines séances!

~

Dans la prochaine partie, nous apprendrons à lancer ce pipeline d'analyse de façon automatiser sur le PSMN.

~

Dans une troisième partie, nous parlerons des analyses statistiques qui peuvent être réalisées sur ces tableaux de comptage.

~

\bibliographystyle{unsrt}
\bibliography{CIRI_formations_RNAseq_part1}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
