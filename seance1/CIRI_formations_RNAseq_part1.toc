\contentsline {section}{\numberline {1}Pr\IeC {\'e}ambule}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Conventions de pr\IeC {\'e}sentation}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Espace de travail}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Introduction}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Contexte de l'\IeC {\'e}tude}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Donn\IeC {\'e}es fournies}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Description des fichiers de donn\IeC {\'e}es}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Contr\IeC {\^o}les qualit\IeC {\'e}s, \textit {fastQC}}{5}{section.3}
\contentsline {section}{\numberline {4}Trimming}{6}{section.4}
\contentsline {section}{\numberline {5}Mapping et quantification}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}G\IeC {\'e}nome et annotation}{8}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Format des fichiers}{8}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Principe de l'analyse}{9}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {5.2}Alignement des donn\IeC {\'e}es de RNAseq sur le g\IeC {\'e}nome}{9}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Construction de l\IeC {\textquoteright }index g\IeC {\'e}nomique}{9}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Alignement des reads}{10}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Visualisation des alignements}{11}{subsubsection.5.2.3}
\contentsline {subsection}{\numberline {5.3}Comptages des nombres de lectures par g\IeC {\`e}ne}{12}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusion}{13}{section.6}
\contentsline {section}{References}{13}{section*.2}
