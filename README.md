# Intro

## Utilisation de machines virtuelles sur le cloud de l'IFB.

**Instructions pour lancer la machine virtuelle en https:**

https://biosphere.france-bioinformatique.fr/cloudweb/login/

- Rendez-vous sur la page du catalogue des appliances sur le site de biosphere (**RAINBIO**).

- Trouvez l'appliance **Formation CIRI**

- Cliquez sur configurer
-- Choisissez un nom de votre choix
-- Vous pouvez configurer le gabarit et choisir le cloud sur lequel vous voulez lancer votre machine virtuelle. Pour ce TP, vous n'avez pas besoin de lancer une très grosse machine: Vous pouvez laissez ces options par défaut.

- Lancer la VM

- Rendez-vous sur l'onglet **myVM**, vous devez trouver une ligne correspondant à votre VM. Quand la machine est prête, vous obtenez un lien *https* et *Params*, dans la colonne onglet. **Attention, cela peut prendre plusieurs (dizaines de) minutes.**

- Quand la machine est intallée, cliquez sur le lien *https*, entrez les identifiants et motes de passes indiqués dans *Params*.

Vous avez maintenant accès à un environnement Rstudio (qui servira pour le TD3), mais également à un terminal UNIX, grace auquel nous allons effectuer toutes les commandes de cette séance.

*remarque: Il ne faut pas confondre la console R est son invite de commande ">", dans laquelle on envoie les commande en langage R, et le terminal (invite "$") dans lequel on écrit des commandes bash}*

**Un quota de ressource est alloué à la formation, pensez à fermer les VMs entre 2 séances et (surtout) à la fin de la formation (après avoir récupérez les données que vous souhaitez conserver).**

# Séance 0

Introduction à Unix et à R.

## Unix

## R

# Séance 1

Analyse de reads bruts de RNAseq, filtrage, mapping et quantification.

# Séance 2

Analyse de données de RNAseq au moyen d'un pipeline sur le PSMN.

# Séance 3

Analyse d'expression différentielle
